package com.example.service;

import com.example.mapper.PeopleMapper;
import com.example.pojo.People;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2020/11/18 0018.
 */
@Service
public class PeopleService {
    @Resource
    private PeopleMapper mapper;

    public List<People> getAllPeople() {
        return mapper.selectAllPeople();
    }

}
