package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created by Administrator on 2020/11/18 0018.
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.example.mapper")
public class PeopleServer {
    public static void main(String[] args) {
        SpringApplication.run(PeopleServer.class,args);
        System.out.println("PeopleServer==============启动成功！！！");
    }
}
