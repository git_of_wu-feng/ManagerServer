package com.example.controller;

import com.example.pojo.People;
import com.example.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2020/11/18 0018.
 */
@Controller
public class PeopleController {
    @Resource
    private PeopleService peopleService;

    @RequestMapping("getAll")
    @ResponseBody
    public List<People> getAll(){
        return peopleService.getAllPeople();
    }

    @RequestMapping("/")
    public String getA(){
        return "welcome";
    }
}
