package com.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by Administrator on 2020/11/18 0018.
 */
@SpringBootApplication
@EnableEurekaServer
public class EuerkaServer {
    public static void main(String[] args) {
        SpringApplication.run(EuerkaServer.class,args);
        System.out.println("EuerkaServer============启动成功！！！");
    }

}
