package com.example.controller;

import com.example.pojo.Kpi;
import com.example.service.KpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2020/11/18 0018.
 */
@Controller
public class KpiController {
    @Autowired
    private KpiService kpiService;
    @RequestMapping("getAll")
    @ResponseBody
    public List<Kpi> getAll(){
        return kpiService.getAllKpi();
    }


}

