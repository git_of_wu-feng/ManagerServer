package com.example.mapper;

import com.example.pojo.Kpi;

import java.util.List;

public interface KpiMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Kpi record);

    int insertSelective(Kpi record);

    Kpi selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Kpi record);

    int updateByPrimaryKey(Kpi record);

    /*    查全部    */
    List<Kpi> selectAllKpi();
}