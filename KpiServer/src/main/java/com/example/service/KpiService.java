package com.example.service;

import com.example.mapper.KpiMapper;
import com.example.pojo.Kpi;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2020/11/18 0018.
 */

@Service
public class KpiService {
    @Resource
    private KpiMapper kpiMapper;
    public List<Kpi> getAllKpi(){
        return kpiMapper.selectAllKpi();
    }
}
