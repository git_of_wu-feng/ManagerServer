package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created by Administrator on 2020/11/18 0018.
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.example.mapper")
public class KpiServer {
    public static void main(String[] args) {
        SpringApplication.run(KpiServer.class,args);
        System.out.println("KpiServer==============启动成功！！！");
    }

}
