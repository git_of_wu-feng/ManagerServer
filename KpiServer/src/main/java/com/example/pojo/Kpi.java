package com.example.pojo;

public class Kpi {
    private Integer id;

    private String type;

    private Double resultmoney;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Double getResultmoney() {
        return resultmoney;
    }

    public void setResultmoney(Double resultmoney) {
        this.resultmoney = resultmoney;
    }
}